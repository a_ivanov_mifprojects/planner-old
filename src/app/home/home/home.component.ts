import {Component, OnInit} from '@angular/core';
import {LayoutService} from '../../layout/base-layout/layout.service';
import {LayoutContent} from '../../layout/layout-content.enum';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private layoutService: LayoutService) {
  }

  ngOnInit() {
    this.updateLayout();
  }

  updateLayout() {
    this.layoutService.updateLayoutState({content: LayoutContent.Centered});
  }
}
