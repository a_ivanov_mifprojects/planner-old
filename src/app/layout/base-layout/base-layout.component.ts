import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {LayoutService} from './layout.service';
import {LayoutStates} from './layout';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BaseLayoutComponent implements OnInit {
  private layoutState: LayoutStates;

  constructor(private readonly layoutService: LayoutService, private readonly changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.listenLayoutStates();
  }

  private listenLayoutStates() {
    this.layoutService.getlayoutStates().subscribe(states => {
      this.layoutState = states;
      this.changeDetectorRef.markForCheck();
    });
  }
}
