import {Directive, HostBinding, Input} from '@angular/core';
import {LayoutContent} from '../layout-content.enum';

@Directive({
  selector: '[appLayout]'
})
export class LayoutDirective {

  @Input() content = LayoutContent.Hidden;

  @HostBinding('attr.data-content')
  get contentState() {
    return this.parseEnumValueAsString(LayoutContent, this.content);
  }

  private parseEnumValueAsString<T>(enumerator: T, value: any) {
    return (typeof value === 'number' ? enumerator[value] : value) as string;
  }
}
