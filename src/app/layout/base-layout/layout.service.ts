import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {LayoutStates} from './layout';
import {LayoutContent} from '../layout-content.enum';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  private layoutStatesChange$ = new BehaviorSubject<LayoutStates>(this.getInitialLayoutStates());

  getlayoutStates() {
    return this.layoutStatesChange$.asObservable();
  }

  updateLayoutState(state: LayoutStates) {
    this.layoutStatesChange$.next({content: state.content, });
  }

  private getInitialLayoutStates() {
    return {content: LayoutContent.Hidden};
  }
}
