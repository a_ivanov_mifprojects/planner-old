import {LayoutContent} from '../layout-content.enum';

export interface LayoutStates {
  content: LayoutContent.Full | LayoutContent.Centered | LayoutContent.Hidden;
}
