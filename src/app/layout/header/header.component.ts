import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {HeaderService} from './header.service';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  backgroundColor: string;

  constructor(private readonly headerService: HeaderService) {
  }

  ngOnInit() {
    this.listenBackgroundColor();
  }

  listenBackgroundColor() {
    this.headerService.getBackgroundColor().pipe(
      filter(color => color !== null)
    )
      .subscribe(backgroundColor => {
        this.backgroundColor = backgroundColor;
      });
  }

}
