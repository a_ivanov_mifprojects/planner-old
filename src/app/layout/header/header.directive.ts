import {Directive, HostBinding, Input} from '@angular/core';
import {SafeStyle} from '@angular/platform-browser';

@Directive({
  selector: '[appHeader]'
})
export class HeaderDirective {
  @Input() backgroundColor: string;

  @HostBinding('style.background-color') get style(): SafeStyle {
    return this.backgroundColor;
  }
}
