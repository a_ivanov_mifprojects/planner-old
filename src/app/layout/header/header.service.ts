import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  private backgroundColor = new BehaviorSubject<string>(null);

  getBackgroundColor() {
    return this.backgroundColor.asObservable();
  }
}
