export enum LayoutContent {
  Full,
  Centered,
  Hidden
}
