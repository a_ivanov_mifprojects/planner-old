import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './ui/button/button.component';
import { LinkComponent } from './ui/link/link.component';



@NgModule({
  declarations: [ButtonComponent, LinkComponent],
  exports: [
    ButtonComponent,
    LinkComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
