import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button, button[appButton]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
