import {Component, HostBinding, Input} from '@angular/core';
import {SafeStyle} from '@angular/platform-browser';

@Component({
  selector: 'app-link, a[appLink]',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent {
  @Input() backgroundColor: string;

  @HostBinding('style.background-color') get style(): SafeStyle {
    return this.backgroundColor;
  }
}
